#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "Player.h"

class Game {
private:
    // Declare window and renderer objects
    SDL_Window* gameWindow;
    SDL_Renderer* gameRenderer;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture;
    
    //Declare Player
    Player* player;

    // Window control 
    bool quit; 
    
    // Keyboard
    const Uint8 *keyStates;

    //Game loop methods
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    //Constructor
    Game();
    ~Game();

    //Methods
    bool init();
    void runGameLoop();

    //Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 600;

};

#endif